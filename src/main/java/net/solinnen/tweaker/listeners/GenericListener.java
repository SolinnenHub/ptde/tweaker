package net.solinnen.tweaker.listeners;

import fr.xephi.authme.events.LoginEvent;
import io.papermc.lib.PaperLib;
import net.luckperms.api.node.Node;
import net.solinnen.tweaker.Tweaker;
import net.solinnen.tweaker.Util;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

import static java.lang.Math.max;
import static java.lang.Math.sqrt;
import static net.solinnen.tweaker.Tweaker.lpApi;
import static net.solinnen.tweaker.Tweaker.operators;
import static net.solinnen.tweaker.Util.squaredDistanceFromZeroFlat;

public class GenericListener implements Listener {

    private final World w_bonfire;
    private final HashSet<UUID> teleportingPlayers = new HashSet<>();
    private final UUID w_world_uid;
    private final UUID w_world_nether_uid;
    private final UUID w_bonfire_uid;
    private final UUID w_bonfire_nether_uid;
    private final Location world_world_spawn_location;
    private boolean cauldron_fulfilling_lock = false;
    private final Tweaker inst;

    private final String lp_group_player = "group.player";

    private final HashSet<UUID> playersWithEnabledResourcePacks = new HashSet<>(100);
    private final String kickMessageResourcePack =
            ChatColor.DARK_RED+"Для игры на сервере вы должны включить ресурс-пак.\n\n" +
            ChatColor.WHITE+"Сетевая игра"+ChatColor.GRAY+" → "+
            ChatColor.WHITE+"Сервер"+ChatColor.GRAY+" → "+
            ChatColor.WHITE+"Настроить"+ChatColor.GRAY+" → "+
            ChatColor.WHITE+"Наборы ресурсов"+ChatColor.GRAY+" → "+
            ChatColor.GREEN+"Включены";


    public GenericListener(Tweaker inst) {
        this.inst = inst;

        this.w_bonfire = Objects.requireNonNull(inst.getServer().getWorld("Bonfire"));
        this.w_world_uid = Objects.requireNonNull(inst.getServer().getWorld("world")).getUID();
        this.w_world_nether_uid = Objects.requireNonNull(inst.getServer().getWorld("world_nether")).getUID();
        this.w_bonfire_uid = this.w_bonfire.getUID();
        this.w_bonfire_nether_uid = Objects.requireNonNull(inst.getServer().getWorld("Bonfire_nether")).getUID();

        world_world_spawn_location = Objects.requireNonNull(inst.getServer().getWorld("world")).getSpawnLocation();

        inst.getServer().getScheduler().scheduleSyncRepeatingTask(inst, () -> {
            for (Player p : inst.getServer().getOnlinePlayers()) { // порталы
                if (p.getWorld().getUID().equals(w_bonfire_uid)) {
                    if ((p.getLocation().getZ() <= 3.3) && (p.getLocation().getX() > 7) && (p.getLocation().getX() < 10)) {
                        UUID uuid = p.getUniqueId();
                        if (!teleportingPlayers.contains(uuid)) {
                            teleportingPlayers.add(uuid);
                            p.setVelocity(new Vector(0, 0.2, -0.9));
                            Bukkit.getScheduler().runTaskLater(inst, () -> {
                                int counter = 0;
                                Location randomLocation = world_world_spawn_location.clone().add(Util.randVector(100));
                                while (randomLocation.clone().add(0,-3,0).getBlock().isPassable() && counter < 8) {
                                    randomLocation = world_world_spawn_location.clone().add(Util.randVector(100));
                                    counter++;
                                }
                                PaperLib.teleportAsync(p, randomLocation).thenRun(new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        p.playSound(p.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, SoundCategory.AMBIENT, 1, 1);
                                        teleportingPlayers.remove(uuid);

                                        validatePlayer(p);
                                    }
                                });
                            }, 6);
                        }
                    }
                }
            }
        }, 1, 15);
    }

    private void validatePlayer(Player p) {
        if (!p.hasPermission(lp_group_player)) {
            lpApi.getUserManager().modifyUser(p.getUniqueId(), user -> {
                user.data().add(Node.builder(lp_group_player).build());
            });
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    private void onPlayerResourcePackStatusEvent(PlayerResourcePackStatusEvent e) {
        Player p = e.getPlayer();
        PlayerResourcePackStatusEvent.Status status = e.getStatus();
        inst.logger.info(p.getName()+"'s resource pack status event: " + status);
        if (status == PlayerResourcePackStatusEvent.Status.DECLINED) {
            if (!operators.contains(p.getUniqueId())) {
                p.sendMessage(ChatColor.RED+"Для игры на сервере вы должны включить ресурс-пак. Сейчас вы будете кикнуты.");
                Bukkit.getScheduler().runTaskLater(inst, () -> p.kickPlayer(kickMessageResourcePack), 55);
            }
        } else if (status.equals(PlayerResourcePackStatusEvent.Status.ACCEPTED) ||
                status.equals(PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED)) {
            playersWithEnabledResourcePacks.add(p.getUniqueId());
        } else if (status.equals(PlayerResourcePackStatusEvent.Status.FAILED_DOWNLOAD)) {
            if (!playersWithEnabledResourcePacks.contains(p.getUniqueId())) {
                playersWithEnabledResourcePacks.add(p.getUniqueId());
                p.setResourcePack(inst.emptyResourcePackUrl);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onPlayerChangedWorldEvent(PlayerChangedWorldEvent e) {
        Player p = e.getPlayer();
        if (p.getWorld().getUID().equals(w_bonfire_uid)) {
            Bukkit.getScheduler().runTaskLater(inst, () ->
                    p.playSound(p.getLocation(), Sound.MUSIC_DISC_13, SoundCategory.MUSIC, 1500, 1), 30);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onPlayerKickEvent(PlayerKickEvent e) {
        e.setReason(
                ChatColor.GOLD+"Minecraft: Prepare To Die Edition\n\n\n"+
                        ChatColor.DARK_RED+e.getReason()+
                        ChatColor.WHITE+"\n\n\nОстались вопросы? Пиши "+
                        ChatColor.BLUE+ChatColor.UNDERLINE+"t.me/minecraft_ptde");
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onPlayerPortalEvent(PlayerPortalEvent e) {
        World w = e.getFrom().getWorld();
        if (w != null) {
            if (w.getUID().equals(w_bonfire_uid)) {
                e.setCancelled(true);
            }
        }
    }

    // запрещаем брать книжки с кафедры
    @EventHandler(priority = EventPriority.MONITOR)
    private void onPlayerTakeLecternBookEvent(PlayerTakeLecternBookEvent e) {
        World w = e.getLectern().getWorld();
        if (w.getUID().equals(w_bonfire_uid)) {
            if (!e.getPlayer().isOp()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    private void onChunkLoadEvent(ChunkLoadEvent e) { // выпиливаем лазуритовую руду с карты
        if (!e.isNewChunk()) {
            return;
        }

        Chunk chunk = e.getChunk();
        for (int y = 0; y <= 32; y++) {
            for (int x = 0; x <= 15; x++) {
                for (int z = 0; z <= 15; z++) {
                    Block block = chunk.getBlock(x, y, z);
                    if (block.getType() == Material.LAPIS_ORE) {
                        block.setType(Material.STONE);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission(lp_group_player)) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.sendMessage(ChatColor.YELLOW+p.getName()+" присоединился к игре");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onLoginEvent(LoginEvent e) {
        Player p = e.getPlayer();
        if (!playersWithEnabledResourcePacks.contains(p.getUniqueId())) {
            inst.logger.info("Attempt to set resource pack for " + p.getName());
            p.setResourcePack(inst.resourcePackUrl);
        }
        UUID worldUid = p.getWorld().getUID();
        if (!(worldUid.equals(w_world_uid) ||
                worldUid.equals(w_bonfire_uid) ||
                worldUid.equals(w_bonfire_nether_uid) ||
                worldUid.equals(w_world_nether_uid))) {
            p.teleport(w_bonfire.getSpawnLocation());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onPlayerQuitEvent(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        UUID uuid = p.getUniqueId();
        teleportingPlayers.remove(uuid);
        playersWithEnabledResourcePacks.remove(uuid);

        if (p.hasPermission(lp_group_player)) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.sendMessage(ChatColor.YELLOW+p.getName()+" покинул игру");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        event.setFormat(ChatColor.GRAY + "%1$s:"+ChatColor.WHITE+" %2$s");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntitySpawnEvent(EntitySpawnEvent event) {
        if (event.getEntity().getType().equals(EntityType.PHANTOM)) {
            event.setCancelled(true);
        }
    }

    // Множитель урона в аду
    @EventHandler(priority = EventPriority.NORMAL)
    private void onEntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntity().getType().equals(EntityType.PLAYER)) {
            UUID eventWorldUid = event.getEntity().getWorld().getUID();
            if (eventWorldUid.equals(w_world_nether_uid) || eventWorldUid.equals(w_bonfire_nether_uid)) {
                if (event.getDamage() < 1000) {
                    double damageMultiplier = 1.1;
                    event.setDamage(event.getDamage() * damageMultiplier);
                }
            }
        }
    }

    // Множители опыта
    @EventHandler(priority = EventPriority.NORMAL)
    private void onEntityDeathEvent(EntityDeathEvent event) {
        if (!(event.getEntity().getType().equals(EntityType.PLAYER))) {
            UUID entityWorldUid = event.getEntity().getWorld().getUID();
            if (entityWorldUid.equals(w_world_nether_uid) || entityWorldUid.equals(w_bonfire_nether_uid)) { // опыт в аду
                float expMultiplierInNether = 1.2f;
                event.setDroppedExp((int) (event.getDroppedExp()*expMultiplierInNether));
            } else if (entityWorldUid.equals(w_world_uid)) { // опыт в обычном мире в зависимости от удаления
                double radiusOfZeroPointExp = 6500;
                double squaredDistanceFlat = squaredDistanceFromZeroFlat(event.getEntity().getLocation());
                double expMultiplierCoefficient = 1 - sqrt(squaredDistanceFlat)/(radiusOfZeroPointExp);
                expMultiplierCoefficient = max(0, expMultiplierCoefficient);
                event.setDroppedExp((int) (event.getDroppedExp()*expMultiplierCoefficient));
            }

            List<ItemStack> items = event.getDrops(); // снятие чар с предметов и удаление зачарованных книг
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getType().equals(Material.ENCHANTED_BOOK)) {
                    items.set(i, new ItemStack(Material.BOOK, 1));
                } else if (items.get(i).getType().equals(Material.LAPIS_LAZULI)) {
                    items.set(i, new ItemStack(Material.GOLD_INGOT, 1));
                } else {
                    for (Map.Entry<Enchantment, Integer> e : items.get(i).getEnchantments().entrySet()) {
                        items.get(i).removeEnchantment(e.getKey());
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onCauldronLevelChangeEvent(CauldronLevelChangeEvent event) {
        if (event.getBlock().getWorld().getUID().equals(w_bonfire_uid)) {
            if (event.getNewLevel() < 3) {
                event.setNewLevel(2);
                if (!cauldron_fulfilling_lock) {
                    cauldron_fulfilling_lock = true;
                    Bukkit.getScheduler().runTaskLater(inst, () -> {
                        Levelled cauldronData = (Levelled) event.getBlock().getBlockData();
                        cauldronData.setLevel(3);
                        event.getBlock().setBlockData(cauldronData);
                        cauldron_fulfilling_lock = false;
                    }, 20 * 6);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onAnvilBroken(BlockBreakEvent event) {
        if (event.getBlock().getWorld().getUID().equals(w_bonfire_uid)) {
            if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                if (event.getBlock().getType().name().contains("ANVIL")) {
                    event.getPlayer().sendMessage("BlockBreakEvent");
                    event.setCancelled(true);
                }
            }
        }
    }
}