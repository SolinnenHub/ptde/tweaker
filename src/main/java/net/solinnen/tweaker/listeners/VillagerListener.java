package net.solinnen.tweaker.listeners;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class VillagerListener implements Listener {

    public VillagerListener() {}

    @EventHandler
    public void onPlayerInteract(PlayerInteractAtEntityEvent e) {
        if (!e.getRightClicked().getType().equals(EntityType.VILLAGER)) {
            return;
        }

        Villager villager = (Villager) e.getRightClicked();
        List<MerchantRecipe> recipes = new ArrayList<>(villager.getRecipes());

        recipes.removeIf(recipe -> {
            ItemStack itemStack = recipe.getResult();
            if (itemStack.getType().equals(Material.ENCHANTED_BOOK)) {
                return true;
            } else if (itemStack.getType().equals(Material.LAPIS_LAZULI)) {
                return true;
            } else {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta != null) {
                    return meta.getEnchants().size() != 0;
                }
            }
            return false;
        });

        villager.setRecipes(recipes);
    }
}
