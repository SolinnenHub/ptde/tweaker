package net.solinnen.tweaker.listeners;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public final class NoPluginsCommand implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCommandUse(PlayerCommandPreprocessEvent event) {

        Player player = event.getPlayer();
        String msg = event.getMessage().toLowerCase();

        if (!player.isOp()) {
            if (msg.startsWith("/help")) {
                event.setCancelled(true);

                String sb = ChatColor.BOLD + "Страница помощи:\n" + ChatColor.RESET +
                        ChatColor.GOLD + "  /spawn" + ChatColor.WHITE + " - Телепортация на спавн.\n" +
                        ChatColor.GOLD + "  /home" + ChatColor.WHITE + " - Телепортация домой.\n" +
                        ChatColor.GOLD + "  /sethome" + ChatColor.WHITE + " - Установить точку возрождения.";
                player.sendMessage(sb);
                sb = ChatColor.BOLD + "Наши сообщества:";
                player.sendMessage(sb);

                TextComponent component = new TextComponent(ChatColor.BLUE+"  "+ChatColor.UNDERLINE+"t.me/minecraft_ptde");
                component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,"https://t.me/minecraft_ptde"));
                player.spigot().sendMessage(component);

                component = new TextComponent(ChatColor.BLUE+"  "+ChatColor.UNDERLINE+"vk.com/minecraft_ptde");
                component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,"https://vk.com/minecraft_ptde"));
                player.spigot().sendMessage(component);

                component = new TextComponent(ChatColor.BLUE+"  "+ChatColor.UNDERLINE+"discord.gg/cQD2jQCaeV");
                component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,"https://discord.com/invite/cQD2jQCaeV"));
                player.spigot().sendMessage(component);

            } else if (msg.startsWith("/?")) {
                event.setCancelled(true);
            }
        }
    }
}

