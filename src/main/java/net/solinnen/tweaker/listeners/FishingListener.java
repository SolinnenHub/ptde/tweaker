package net.solinnen.tweaker.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.Math.max;
import static java.lang.Math.sqrt;
import static net.solinnen.tweaker.Util.squaredDistanceFromZeroFlat;

public class FishingListener implements Listener {

    private final List<Material> trash;

    public FishingListener() {
        this.trash = Arrays.asList(
                Material.WOODEN_SHOVEL,
                Material.WOODEN_SWORD,
                Material.JUNGLE_BOAT,
                Material.JUNGLE_LOG,
                Material.STONE_AXE,
                Material.BUCKET,
                Material.BOOK,
                Material.BOOK,
                Material.WOODEN_AXE,
                Material.SADDLE,
                Material.LEATHER_HORSE_ARMOR,
                Material.FLOWER_POT,
                Material.ROTTEN_FLESH,
                Material.KELP,
                Material.CHAIN,
                Material.CHAINMAIL_HELMET,
                Material.CHAINMAIL_CHESTPLATE,
                Material.CHAINMAIL_LEGGINGS,
                Material.CHAINMAIL_BOOTS,
                Material.DARK_OAK_LOG,
                Material.OAK_LOG,
                Material.GLASS_BOTTLE,
                Material.HONEY_BOTTLE,
                Material.CHARCOAL,
                Material.FISHING_ROD,
                Material.LEATHER,
                Material.LEATHER_HELMET,
                Material.LEATHER_LEGGINGS,
                Material.LEATHER_CHESTPLATE,
                Material.LEATHER_BOOTS,
                Material.STONE_HOE,
                Material.IRON_SWORD,
                Material.IRON_SHOVEL,
                Material.COMPASS,
                Material.SHEARS,
                Material.TRIDENT,
                Material.FERMENTED_SPIDER_EYE,
                Material.PHANTOM_MEMBRANE,
                Material.FEATHER,
                Material.STRING,
                Material.TURTLE_EGG,
                Material.RABBIT_HIDE,
                Material.BLAZE_POWDER,
                Material.SHIELD,
                Material.CLOCK,
                Material.DIAMOND
        );
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerFishEvent(PlayerFishEvent e) {
        double radiusOfZeroPointExp = 3000;
        double squaredDistanceFlat = squaredDistanceFromZeroFlat(e.getPlayer().getLocation());
        double expMultiplierCoefficient = (1 - 1/(radiusOfZeroPointExp)*sqrt(squaredDistanceFlat)) / 4d;
        expMultiplierCoefficient = max(0, expMultiplierCoefficient);
        e.setExpToDrop((int) (e.getExpToDrop()*expMultiplierCoefficient));

        if (e.getCaught() != null) {
            ItemStack itemStack = ((Item) e.getCaught()).getItemStack();
            if (itemStack.getType().equals(Material.ENCHANTED_BOOK)) {
                ((Item) e.getCaught()).setItemStack(new ItemStack(trash.get(new Random().nextInt(trash.size()))));
            } else if (itemStack.getType().equals(Material.LAPIS_LAZULI)) {
                ((Item) e.getCaught()).setItemStack(new ItemStack(trash.get(new Random().nextInt(trash.size()))));
            } else {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta != null) {
                    if (meta.getEnchants().size() > 0) {
                        ((Item) e.getCaught()).setItemStack(new ItemStack(trash.get(new Random().nextInt(trash.size()))));
                    }
                }
            }
        }
    }
}
