package net.solinnen.tweaker;

import org.bukkit.Location;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

public abstract class Util {

    public static double squaredDistanceFromZeroFlat(Location destination) {
        return NumberConversions.square(destination.getX()) +
                NumberConversions.square(destination.getZ());
    }

    public static int randRange(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static Vector randVector(int radius) {
        return new Vector(
                randRange(0, radius) - 0.5,
                0,
                randRange(0, radius) - 0.5
        );
    }
}
