package net.solinnen.tweaker;

import fr.xephi.authme.api.v3.AuthMeApi;
import net.luckperms.api.LuckPerms;
import net.solinnen.tweaker.listeners.FishingListener;
import net.solinnen.tweaker.listeners.GenericListener;
import net.solinnen.tweaker.listeners.NoPluginsCommand;
import net.solinnen.tweaker.listeners.VillagerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Logger;

public class Tweaker extends JavaPlugin {

    public static Tweaker inst;
    public static LuckPerms lpApi;
    public AuthMeApi authme;
    public Logger logger;

    public String resourcePackUrl;
    public String emptyResourcePackUrl;

    public static final String N_HIDE_SCOREBOARD_TEAM_NAME = "nHideTeam";
    public static final HashSet<UUID> operators = new HashSet<>();

    @Override
    public void onEnable() {
        logger = getLogger();
        inst = this;
        authme = AuthMeApi.getInstance();

        loadConfiguration();

        // Фичу отключил, потому что в tab-меню ники тоже пропали. Внезапно.
        /*ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

        String emptyName = " ";

        // сокрытие ников на уровне пакетов
        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.LOWEST, PacketType.Play.Server.PLAYER_INFO) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if (event.getPacket().getPlayerInfoAction().read(0) != EnumWrappers.PlayerInfoAction.ADD_PLAYER) {
                            return;
                        }
                        List<PlayerInfoData> newPlayerInfoDataList = new ArrayList<>();
                        List<PlayerInfoData> playerInfoDataList = event.getPacket().getPlayerInfoDataLists().read(0);
                        for (PlayerInfoData playerInfoData : playerInfoDataList) {
                            if (playerInfoData == null || playerInfoData.getProfile() == null || Bukkit.getPlayer(playerInfoData.getProfile().getUUID()) == null) { //Unknown Player
                                newPlayerInfoDataList.add(playerInfoData);
                                continue;
                            }
                            WrappedGameProfile profile = playerInfoData.getProfile();
                            WrappedGameProfile namelessProfile = profile.withName(emptyName);
                            namelessProfile.getProperties().putAll("textures", profile.getProperties().get("textures"));
                            PlayerInfoData newPlayerInfoData = new PlayerInfoData(
                                    namelessProfile,
                                    playerInfoData.getLatency(),
                                    playerInfoData.getGameMode(),
                                    playerInfoData.getDisplayName()
                            );
                            newPlayerInfoDataList.add(newPlayerInfoData);
                        }
                        event.getPacket().getPlayerInfoDataLists().write(0, newPlayerInfoDataList);
                    }
                });*/

        registerEvents();
        Objects.requireNonNull(getCommand("tweaker")).setExecutor(new CommandProcess());

        /*ScoreboardManager scoreboard = Bukkit.getScoreboardManager();
        if (scoreboard != null) {
            Scoreboard score = scoreboard.getMainScoreboard();
            Team team = score.getTeam(N_HIDE_SCOREBOARD_TEAM_NAME);
            if (team == null) {
                team = score.registerNewTeam(N_HIDE_SCOREBOARD_TEAM_NAME);
                team.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.NEVER);
            }
            if (!team.hasEntry(emptyName)) {
                team.addEntry(emptyName);
            }
            for (OfflinePlayer p : getServer().getOperators()) {
                operators.add(p.getUniqueId());
            }
        }*/

        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            lpApi = provider.getProvider();
        }
    }

    public void loadConfiguration() {
        try {
            File cfgFile = new File(getDataFolder() + File.separator + "config.yml");
            YamlConfiguration config = YamlConfiguration.loadConfiguration(cfgFile);
            config.addDefault("resourcePackUrl", "http://ptde.ru/http_shared/PTDE.zip");
            config.addDefault("emptyResourcePackUrl", "http://ptde.ru/http_shared/Empty.zip");
            config.options().copyDefaults(true);
            config.save(cfgFile);

            this.resourcePackUrl = Objects.requireNonNull(config.getString("resourcePackUrl"));
            this.emptyResourcePackUrl = Objects.requireNonNull(config.getString("emptyResourcePackUrl"));
        } catch (IOException e) {
            logger.warning(ChatColor.DARK_RED+"Can't load config.");
        }
    }

    public void unregisterEvents() {
        HandlerList.unregisterAll(this);
    }

    public void registerEvents() {
        PluginManager plugman = getServer().getPluginManager();
        plugman.registerEvents(new GenericListener(this), this);
        plugman.registerEvents(new VillagerListener(), this);
        plugman.registerEvents(new FishingListener(), this);
        plugman.registerEvents(new NoPluginsCommand(), this);
    }

    public void resetEvents() {
        unregisterEvents();
        registerEvents();
    }
}