package net.solinnen.tweaker;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandProcess implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if (!cmd.getName().equalsIgnoreCase("tweaker")) {
            return false;
        }
        if (args.length < 1) {
            return false;
        }
        if (!sender.isOp()) {
            return true;
        }
        String prefix = ChatColor.GOLD+"["+Tweaker.inst.getName()+"] "+ChatColor.WHITE;
        switch (args[0]) {
            case "enable":
                Tweaker.inst.resetEvents();
                sender.sendMessage(prefix + "Event listener enabled.");
                return true;
            case "disable":
                Tweaker.inst.unregisterEvents();
                sender.sendMessage(prefix + "Event listener disabled.");
                return true;
            case "reload":
                Tweaker.inst.loadConfiguration();
                sender.sendMessage(prefix + "Reloaded.");
                Tweaker.inst.resetEvents();
                sender.sendMessage(prefix + "Event listener enabled.");
                return true;
        }
        return true;
    }
}


